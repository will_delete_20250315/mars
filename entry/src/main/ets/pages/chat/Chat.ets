/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { GlobalContext, StnLogic } from '@ohos/mars';
import MarsServiceNative from '../wrapper/service/MarsServiceNative';
import MarsServiceProfile from '../wrapper/service/MarsServiceProfile';
import prompt from '@system.prompt';
import marsnapi from 'libmarsnapi.so';
import getResourceStr from '../sample/utils/ResourceString';

const showToast = (str: string) => {
  console.log('showToast:' + str)
  prompt.showToast({
    message: str,
    duration: 2000,
  });
}

@Entry
@Component
struct Demo {
  scroller: Scroller = new Scroller()
  sendCount: number = 0;
  receiveCount: number = 0;
  randomKey: string = '';

  public showResult(result: number): void {
    if (0 >= result) {
      showToast("Sucess");
    } else {
      showToast("Error");
    }
  }

  aboutToAppear() {

  }

  aboutToDisappear() {

  }

  build() {
    Column({ space: 10 }) {
      Text('Chat Demo')
        .fontSize(30)
        .fontWeight(FontWeight.Bold)
      Scroll() {
        Column({ space: 5 }) {
          Button(`BaseEvent ${getResourceStr($r('app.string.Create'))} onCreate`).onClick(event => {
            let result: number = marsnapi.BaseEvent_onCreate();
            this.showResult(result);
            console.log("BaseEvent_onCreate   result = " + result);
          })
          Button('context.applicationInfo.name').onClick(event => {
            let context = GlobalContext.getContext().getValue('context') as Context;
            console.info('context.applicationInfo.name==' + context.applicationInfo.name)
          })
          Button(`BaseEvent ${getResourceStr($r('app.string.Test'))} onInitConfigBeforeOnCreate`).onClick(event => {
            let packer_encoder_version: number = 100;
            let result: number = marsnapi.BaseEvent_onInitConfigBeforeOnCreate(packer_encoder_version);
            this.showResult(result);
            console.log("BaseEvent_onInitConfigBeforeOnCreate  result = " + result);
          })
          Button(`BaseEvent ${getResourceStr($r('app.string.Test'))} onDestroy`).onClick(event => {
            console.log("BaseEvent_onDestroy ");
            let result: number = marsnapi.BaseEvent_onDestroy();
            this.showResult(result);
            console.log("BaseEvent_onDestroy  result = " + result);
          })
          Button(`BaseEvent ${getResourceStr($r('app.string.Test'))} onForeground`).onClick(event => {
            console.log("BaseEvent_onForeground ");
            let result: number = marsnapi.BaseEvent_onForeground(true);
            this.showResult(result);
            console.log("BaseEvent_onForeground  result = " + result);
          })
          Button(`BaseEvent ${getResourceStr($r('app.string.Test'))} onNetworkChange`).onClick(event => {
            console.log("BaseEvent_onNetworkChange ");
            let result: number = marsnapi.BaseEvent_onNetworkChange();
            this.showResult(result);
            console.log("BaseEvent_onNetworkChange  result = " + result);
          })
          Button(`BaseEvent ${getResourceStr($r('app.string.Test'))} onSingalCrash`).onClick(event => {
            console.log("BaseEvent_onSingalCrash ");
            let sig: number = 1;
            let result: number = marsnapi.BaseEvent_onSingalCrash(sig);
            this.showResult(result);
            console.log("BaseEvent_onSingalCrash  result = " + result);
          })
          Button(`BaseEvent ${getResourceStr($r('app.string.Test'))} onExceptionCrash`).onClick(event => {
            console.log("BaseEvent_onExceptionCrash ");
            let result: number = marsnapi.BaseEvent_onExceptionCrash();
            this.showResult(result);
            console.log("BaseEvent_onExceptionCrash  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} setLonglinkSvrAddr`).onClick(event => {
            console.log("StnLogic_setLonglinkSvrAddr  ");
            let profile: MarsServiceProfile = MarsServiceNative.gFactory.createMarsServiceProfile();
            let result: number = marsnapi.StnLogic_setLonglinkSvrAddr(profile.longLinkHost(), profile.longLinkPorts(), "");
            this.showResult(result);
            console.log("StnLogic_setLonglinkSvrAddr   result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} setShortlinkSvrAddr`).onClick(event => {
            console.log("StnLogic_setShortlinkSvrAddr ");
            let profile: MarsServiceProfile = MarsServiceNative.gFactory.createMarsServiceProfile();
            let result: number = marsnapi.StnLogic_setShortlinkSvrAddr(profile.shortLinkPort(), "");
            this.showResult(result);
            console.log("StnLogic_setShortlinkSvrAddr  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} setDebugIP`).onClick(event => {
            console.log("StnLogic_setDebugIP ");
            let result: number = marsnapi.StnLogic_setDebugIP("host", "debug_ip");
            this.showResult(result);
            console.log("StnLogic_setDebugIP  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} setBackupIPs`).onClick(event => {
            console.log("StnLogic_setBackupIPs ");
            let result: number = marsnapi.StnLogic_setBackupIPs("host", ["backupip_list"]);
            this.showResult(result);
            console.log("StnLogic_setBackupIPs  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} startTask`).onClick(event => {
            let shortLinkHostList = new Array("shortLink01", "shortLink02", "shortLink03");
            let result: number = marsnapi.StnLogic_startTask(new StnLogic.Task(StnLogic.Task.EShort, 0, "cgi", shortLinkHostList));
            this.showResult(result);
            console.log("StnLogic_startTask  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} stopTask`).onClick(event => {
            console.log("StnLogic_stopTask ");
            let result: number = marsnapi.StnLogic_stopTask();
            this.showResult(result);
            console.log("StnLogic_stopTask  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} hasTask`).onClick(event => {
            console.log("StnLogic_hasTask ");
            let taskid: number = 100;
            let result: number = marsnapi.StnLogic_hasTask(taskid);
            this.showResult(result);
            console.log("StnLogic_hasTask  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} redoTask`).onClick(event => {
            console.log("StnLogic_redoTask ");
            let shortLinkHostList = new Array("shortLink01", "shortLink02", "shortLink03");
            let result: number = marsnapi.StnLogic_redoTask(new StnLogic.Task(StnLogic.Task.EShort, 0, "cgi", shortLinkHostList));
            this.showResult(result);
            console.log("StnLogic_redoTask  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} clearTask`).onClick(event => {
            console.log("StnLogic_clearTask ");
            let shortLinkHostList = new Array("shortLink01", "shortLink02", "shortLink03");
            let result: number = marsnapi.StnLogic_clearTask(new StnLogic.Task(StnLogic.Task.EShort, 0, "cgi", shortLinkHostList));
            this.showResult(result);
            console.log("StnLogic_clearTask  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} makesureLongLinkConnected`).onClick(event => {
            console.log("StnLogic_makesureLongLinkConnected ");
            let result: number = marsnapi.StnLogic_makesureLongLinkConnected();
            this.showResult(result);
            console.log("StnLogic_makesureLongLinkConnected  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} setSignallingStrategy`).onClick(event => {
            console.log("StnLogic_setSignallingStrategy ");
            let result: number = marsnapi.StnLogic_setSignallingStrategy();
            this.showResult(result);
            console.log("StnLogic_setSignallingStrategy  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} keepSignalling`).onClick(event => {
            console.log("StnLogic_keepSignalling ");
            let result: number = marsnapi.StnLogic_keepSignalling();
            this.showResult(result);
            console.log("StnLogic_keepSignalling  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} stopSignalling`).onClick(event => {
            console.log("StnLogic_stopSignalling ");
            let result: number = marsnapi.StnLogic_stopSignalling();
            this.showResult(result);
            console.log("StnLogic_stopSignalling  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} setClientVersion`).onClick(event => {
            console.log("StnLogic_setClientVersion ");
            let profile: MarsServiceProfile = MarsServiceNative.gFactory.createMarsServiceProfile();
            let result: number = marsnapi.StnLogic_setClientVersion(profile.productID());
            this.showResult(result);
            console.log("StnLogic_setClientVersion  result = " + result);
          })
          Button(`StnLogic ${getResourceStr($r('app.string.Test'))} genTaskID`).onClick(event => {
            console.log("StnLogic_genTaskID ");
            let shortLinkHostList = new Array("shortLink01", "shortLink02", "shortLink03");
            let result: number = marsnapi.StnLogic_genTaskID(new StnLogic.Task(StnLogic.Task.EShort, 0, "cgi", shortLinkHostList));
            this.showResult(result);
            console.log("StnLogic_genTaskID  result = " + result);
          })
        }.width('100%')
        .margin({ bottom: 150 })
      }
      .scrollable(ScrollDirection.Vertical).scrollBar(BarState.On)
      .scrollBarColor(Color.Gray).scrollBarWidth(30)
    }.gridSpan(10)
  }
}