/*
 * Tencent is pleased to support the open source community by making Mars available.
 * Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
 *
 * Licensed under the MIT License (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://opensource.org/licenses/MIT
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ChatMsgEntity } from './ChatMsgEntity'

export enum Layout {
  None,
  ChattingItemMsgTextLeft,
  ChattingItemMsgTextRight,
}
/**
 * 消息ListView的Adapter.
 *
 * @author way
 */
class ChatMsgViewAdapter {
  private static ITEMCOUNT: number = 2;// 消息类型的总数
  private static IMVT_COM_MSG: number = 0;// 收到对方的消息
  private static IMVT_TO_MSG: number = 1;// 自己发送出去的消息
  private coll: Set<ChatMsgEntity>;// 消息对象数组


  public constructor(coll: Set<ChatMsgEntity>) {
      this.coll = coll;
  }

  public getCount(): number {
      return this.coll.size;
  }

  public getItem(iposition: number): Object {
    let index = 0;
    for (let value of this.coll.values()) {
      if (index == iposition) {
        return value;
      }
    }
    return null;
  }

  public getItemId(position: number): number {
      return position;
  }

  /**
   * 得到Item的类型，是对方发过来的消息，还是自己发送出去的.
   */
  public getItemViewType(position: number): number {
    let index = 0;
    let entity: ChatMsgEntity;
    for (let value of this.coll.values()) {
      if (index == position) {
        entity = value;
      }
    }

    if (entity.getMsgType()) { //收到的消息
      return ChatMsgViewAdapter.IMVT_COM_MSG;
    } else { //自己发送的消息
      return ChatMsgViewAdapter.IMVT_TO_MSG;
    }
  }

  /**
   * Item类型的总数.
   */
  public getViewTypeCount(): number {
      return ChatMsgViewAdapter.ITEMCOUNT;
  }

  public getView(position: number, convertView: Layout): void {
    let index = 0;
    let entity: ChatMsgEntity;
    for (let value of this.coll.values()) {
      if (index == position) {
        entity = value;
      }
    }
    let isComMsg: boolean = entity.getMsgType();

    let viewHolder: InstanceType<typeof ChatMsgViewAdapter.ViewHolder> = null;
    if (convertView == Layout.None) {
      if (isComMsg) {
        convertView = Layout.ChattingItemMsgTextLeft;
      } else {
        convertView = Layout.ChattingItemMsgTextRight;
      }

      viewHolder = new ChatMsgViewAdapter.ViewHolder();
      viewHolder.tvSendTime = AppStorage.Get('tv_sendtime');
      viewHolder.tvUserName = AppStorage.Get('tv_username');
      viewHolder.tvContent = AppStorage.Get('tv_chatcontent');
      viewHolder.isComMsg = isComMsg;
      AppStorage.Set('view_holder', viewHolder);
    } else {
      viewHolder = AppStorage.Get('view_holder');
    }
    viewHolder.tvSendTime = entity.getDate();
    viewHolder.tvUserName = entity.getName();
    viewHolder.tvContent = entity.getMessage();
  }

  static ViewHolder = class {
    public tvSendTime: String;
    public tvUserName: String;
    public tvContent: String;
    public isComMsg: boolean = true;
  }
}

export default ChatMsgViewAdapter;