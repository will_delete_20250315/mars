/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

import { ChatMsgEntity } from './ChatMsgEntity'
import Constants from '../utils/Constants'
import { Observable } from './Observable'

export class ChatDataCore extends Observable {
  public static TAG: String = "ChatDataCore";
  private static inst: ChatDataCore = new ChatDataCore();
  private dataArrays: Map<String, Array<ChatMsgEntity>> = new Map();
  public static CommonEventSubscribeInfo = { events: ['publish_event'] };
  public static mySubscriber = null;

  public static getInstance(): ChatDataCore {
    if (!ChatDataCore.inst) {
      var global = globalThis.exports.default;
      if (global) {
        if (!global.data.charDataCore) {
          global.data.charDataCore = new ChatDataCore();
        }
        ChatDataCore.inst = global.data.charDataCore;
      }
    }
    return ChatDataCore.inst;
  }

  public constructor() {
    super()
  }

  public getTopicDatas(topic: string): Array<ChatMsgEntity> {
    if (!this.dataArrays.has(topic)) {
      this.dataArrays.set(topic, new Array<ChatMsgEntity>());
    }
    console.info("getTopicDatas topic:" + topic + ", datas:" + JSON.stringify(this.dataArrays.get(topic)));
    return this.dataArrays.get(topic);
  }

  public addData(topic: string, entity: ChatMsgEntity) {
    console.info("addData topic:" + topic + ", entity:" + JSON.stringify(entity));
    this.getTopicDatas(topic).push(entity)
    this.notifyObservers();
  }

  /**
   * 发送消息时，获取当前事件.
   *
   * @return 当前时间
   */
  public static getDate(): string {
    let dateTime = new Date();
    let year: number = dateTime.getFullYear(); //当前年
    let month: any = dateTime.getMonth() + 1;
    let date: any = dateTime.getDate();
    let hours: any = dateTime.getHours();
    let minutes: any = dateTime.getMinutes();
    let seconds: any = dateTime.getSeconds();
    let dateStr = year + '-' + month + '-' + date + ' ' + hours + ':' + minutes + ':' + seconds;
    return dateStr;
  }

  public onReceive(action: string, data: Map<string, string>) {
    if (action == Constants.PUSHACTION) {
      let topic = data.get("msgtopic");
      let from = data.get("msgfrom");
      let text = data.get("msgcontent");
      console.info("enter onReceive topic:" + topic + ", from:" + from + ", text:" + text);

      let entity = new ChatMsgEntity();
      entity.setTopic(topic);
      entity.setName(from);
      entity.setDate(ChatDataCore.getDate());
      entity.setMessage(text);
      entity.setMsgType(true);
      this.getTopicDatas(topic).push(entity);

      this.notifyObservers();
    }
  }
}
