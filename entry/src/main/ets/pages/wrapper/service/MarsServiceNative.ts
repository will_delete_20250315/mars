/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

import DebugMarsServiceProfile from './DebugMarsServiceProfile'
import MarsServiceProfile from './MarsServiceProfile'
import MarsServiceProfileFactory from './MarsServiceProfileFactory'

class MarsServiceNative {
  private static readonly TAG: string = "Mars.Sample.MarsServiceNative";

  public onCreate(): void {}

  private static MarsServiceProfileFactory = class {
    public createMarsServiceProfile(): MarsServiceProfile {
      return new DebugMarsServiceProfile();
    }
  };
  public static gFactory: MarsServiceProfileFactory = new MarsServiceNative.MarsServiceProfileFactory();

  public static setProfileFactory(factory: MarsServiceProfileFactory): void {
    MarsServiceNative.gFactory = factory;
  }
}

export default MarsServiceNative;