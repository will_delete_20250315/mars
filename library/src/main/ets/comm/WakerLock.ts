/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import runningLock from '@ohos.runningLock';

class WakerLock {
  public static readonly TAG: string = "MicroMsg.WakerLock";

  public wakeLock: runningLock.RunningLock;
  private timerId: number

  public constructor() {
    // 创建阻止系统休眠的锁
    runningLock.createRunningLock(WakerLock.TAG, runningLock.RunningLockType.BACKGROUND)
      .then(runningLock => {
        console.info('create runningLock success: ' + JSON.stringify(runningLock));
        this.wakeLock = runningLock;
      })
      .catch(error => {
        console.log('create runningLock fail: ' + error);
      })
  }

  protected finalize(): void {
    this.unLock();
  }

  public lock(timeInMills: number): void {
    this.lockNoParam();
    this.timerId = setTimeout(() => {
      this.unLock()
    }, timeInMills)
  }

  public lockNoParam(): void {
    clearTimeout(this.timerId)
    this.wakeLock.lock(0);
  }

  public unLock(): void {
    clearTimeout(this.timerId)
    if (this.wakeLock.isUsed()) {
      this.wakeLock.unlock();
    }
  }

  public isLocking(): Boolean {
    let isUsed = this.wakeLock.isUsed();
    console.info('runningLock used status: ' + isUsed);
    return isUsed;
  }
}

export default WakerLock;
