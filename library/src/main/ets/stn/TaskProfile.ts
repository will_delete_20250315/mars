/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * 网络任务统计信息类
 * Created by caoshaokun on 2016/12/1.
 */

class TaskProfile {

  public taskId: number;
  public cmdId: number;
  public cgi: string;
  public startTaskTime: number;
  public endTaskTime: number;
  public dyntimeStatus: number;
  public errCode: number;
  public errType: number;
  public channelSelect: number;
  public historyNetLinkers: TaskProfile.ConnectProfile[];

  static ConnectProfile = class {
    public startTime: number;
    public dnsTime: number;
    public dnsEndTime: number;
    public connTime: number;
    public connErrCode: number;
    public tryIPCount: number;
    public ip: string;
    public port: number;
    public host: string;
    public ipType: number;
    public disconnTime: number;
    public disconnErrType: number;
    public disconnErrCode: number;

    public toString(): string {
      return "ConnectProfile{" +
        "startTime=" + this.startTime +
        ", dnsTime=" + this.dnsTime +
        ", dnsEndTime=" + this.dnsEndTime +
        ", connTime=" + this.connTime +
        ", connErrCode=" + this.connErrCode +
        ", tryIPCount=" + this.tryIPCount +
        ", ip='" + this.ip + '\'' +
        ", port=" + this.port +
        ", host='" + this.host + '\'' +
        ", ipType=" + this.ipType +
        ", disconnTime=" + this.disconnTime +
        ", disconnErrType=" + this.disconnErrType +
        ", disconnErrCode=" + this.disconnErrCode +
        '}';
    }
  }

  public toString(): string {
    return "TaskProfile{" +
           "taskId=" + this.taskId +
           ", cmdId=" + this.cmdId +
           ", cgi='" + this.cgi + '\'' +
           ", startTaskTime=" + this.startTaskTime +
           ", endTaskTime=" + this.endTaskTime +
           ", dyntimeStatus=" + this.dyntimeStatus +
           ", errCode=" + this.errCode +
           ", errType=" + this.errType +
           ", channelSelect=" + this.channelSelect +
           ", historyNetLinkers=" + this.historyNetLinkers.toString() +
           '}';
  }
}

namespace TaskProfile {
  export type ConnectProfile = typeof TaskProfile.ConnectProfile.prototype;
}

export default TaskProfile;